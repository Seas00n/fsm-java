FSM
> Finite State Machine
>
> 有限状态机

项目中共有4中状态机的实现方式。

* 基于Switch语句实现的有限状态机，代码在master分支
* 基于State模式实现的有限状态机。代码在state-pattern分支
* 基于状态集合实现的有限状态机。代码在collection-state分支
* 基于枚举实现的状态机。代码在enum-state分支
